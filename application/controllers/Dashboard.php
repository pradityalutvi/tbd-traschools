<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Controller {

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Selamat Datang');

		$data['breadcrumbs'] = array(
            array('title' => 'Beranda', 'url' => base_url()),
			array('title' => 'Selamat Datang di Halaman Beranda', 'url' => base_url()),
		);
        
        $this->layout->view('dashboard/home', $data);
    }

}