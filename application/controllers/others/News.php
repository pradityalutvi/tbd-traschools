<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends App_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // header("Access-Control-Allow-Origin: *");
        $this->data = array('title' => 'Berita Pendidikan');

        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Berita Pendidikan', 'News');
        $this->layout->view('others/news/index', $this->data);
    }
}
