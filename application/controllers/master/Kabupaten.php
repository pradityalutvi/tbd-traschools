<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends App_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master/kabupaten_model');
    }

    public function index()
    {
        $this->data = array('title' => 'Data Kabupaten');
        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Kabupaten', 'Kabupaten');
        $this->layout->add_js(base_url('assets/js/master/kabupaten/data_kabupaten.js'));
        $this->layout->view('master/kabupaten/data_kabupaten', $this->data);
    }

    public function get_data_kabupaten()
    {
        echo $this->kabupaten_model->get_data_kabupaten();
    }

    // public function byJson()
    // {
    //     for ($i = 22; $i <= 34; $i++) {

    //         $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/cwilayah/wilayahKabGet?mst_kode_wilayah=' . $i . '0000';
    //         $curl = curl_init();
    //         curl_setopt($curl, CURLOPT_URL, $url);
    //         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //         $result = curl_exec($curl);
    //         curl_close($curl);
    //         $result = $this->clearJSON($result);
    //         $array_wilayah = json_decode($result, true);

    //         // cek ada datanya tidak kalo ada di sinkronkan
    //         if (count($array_wilayah['data']) > 0) {
    //             $this->kabupaten_model->sinkron_data($array_wilayah['data']);
    //         }
    //     }
    //     echo $result;
    // }

    public function sync_data_kemendikbud()
    {
        if (!$this->ion_auth_acl->has_permission('master>kabupaten>sync')) {
            $this->load->view('permission/denied', array());
            return;
        }
        $list_kode_provinsi = $this->kabupaten_model->get_list_kode_provinsi();
        $this->kabupaten_model->delete_data_tabel();
        foreach ($list_kode_provinsi as $row => $data) {
            // var_dump($data['kode_wilayah']);
            // crawl data api untuk data kabupaten
            $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/cwilayah/wilayahKabGet?mst_kode_wilayah=' . $data['kode_wilayah'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($curl);
            curl_close($curl);
            $result = $this->clearJSON($result);
            $array_wilayah = json_decode($result, true);

            // cek ada datanya tidak kalo ada di sinkronkan
            if (count($array_wilayah['data']) > 0) {
                $this->kabupaten_model->sinkron_data($array_wilayah['data']);
            }
        }
        // echo $result;
    }

    public function clearJSON($json_data)
    {
        if (0 === strpos(bin2hex($json_data), 'efbbbf')) {
            return substr($json_data, 3);
        } else {
            return $json_data;
        }
    }
}
