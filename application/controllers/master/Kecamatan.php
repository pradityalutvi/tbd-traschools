<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends App_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master/kecamatan_model');
    }

    public function index()
    {
        $this->data = array('title' => 'Data Kecamatan');
        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Kecamatan', 'Kecamatan');
        $this->layout->add_js(base_url('assets/js/master/kecamatan/data_kecamatan.js'));
        $this->layout->view('master/kecamatan/data_kecamatan', $this->data);
    }

    public function get_data_kecamatan()
    {
        echo $this->kecamatan_model->get_data_kecamatan();
    }

    public function sync_data_kemendikbud()
    {
        if (!$this->ion_auth_acl->has_permission('master>kecamatan>sync')) {
            $this->load->view('permission/denied', array());
            return;
        }
        $list_kode_kabupaten = $this->kecamatan_model->get_list_kode_kabupaten();
        $this->kecamatan_model->delete_data_tabel();
        foreach ($list_kode_kabupaten as $row => $data) {
            // var_dump($data['kode_wilayah']);
            // crawl data api untuk data kecamatan
            $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/cwilayah/wilayahKabGet?mst_kode_wilayah=' . $data['kode_wilayah'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($curl);
            curl_close($curl);
            $result = $this->clearJSON($result);
            $array_wilayah = json_decode($result, true);

            // cek ada datanya tidak kalo ada di sinkronkan
            if (count($array_wilayah['data']) > 0) {
                $this->kecamatan_model->sinkron_data($array_wilayah['data']);
            }
        }
        echo $result;
    }

    public function clearJSON($json_data)
    {
        if (0 === strpos(bin2hex($json_data), 'efbbbf')) {
            return substr($json_data, 3);
        } else {
            return $json_data;
        }
    }
}
