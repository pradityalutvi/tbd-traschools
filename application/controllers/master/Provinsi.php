<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi extends App_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master/provinsi_model');
    }


    public function index()
    {
        $this->data = array('title' => 'Data Provinsi');
        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Provinsi', 'Provinsi');
        $this->data['list_data']   = $this->provinsi_model->get_data_provinsi();
        $this->layout->add_js(base_url('assets/js/master/provinsi/data_provinsi.js'));
        $this->layout->view('master/provinsi/data_provinsi', $this->data);
    }

    public function sync_data_kemendikbud($kode_wil = null)
    {
        if (!$this->ion_auth_acl->has_permission('master>kabupaten>sync')) {
            $this->load->view('permission/denied', array());
            return;
        }
        $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/cwilayah/wilayahKabGet?mst_kode_wilayah=' . $kode_wil;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = $this->clearJSON($result);
        $array_wilayah = json_decode($result, true);

        // cek ada datanya tidak, kalo ada di sinkronkan
        if (count($array_wilayah['data']) > 0) {
            $this->provinsi_model->delete_data_tabel();
            $this->provinsi_model->sinkron_data($array_wilayah['data']);
        }
        echo $result;
    }

    public function clearJSON($json_data)
    {
        if (0 === strpos(bin2hex($json_data), 'efbbbf')) {
            return substr($json_data, 3);
        } else {
            return $json_data;
        }
    }
}
