<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sekolah extends App_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master/sekolah_model');
    }

    public function index()
    {
        $this->data = array('title' => 'Data Sekolah');
        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Sekolah', 'Sekolah');
        $this->layout->add_js(base_url('assets/js/master/sekolah/data_sekolah.js'));
        $this->layout->view('master/sekolah/data_sekolah', $this->data);
    }

    public function get_data_sekolah()
    {
        echo $this->sekolah_model->get_data_sekolah();
    }

    public function byJson()
    {
        $data = file_get_contents(base_url('assets/json/sekolahJateng.json'));
        $data = $this->clearJSON($data);
        $sekolah = json_decode($data, true);
        $this->sekolah_model->sinkron_data($sekolah['data']);
        var_dump('sukses');
    }

    public function sync_data_kemendikbud()
    {
        if (!$this->ion_auth_acl->has_permission('master>sekolah>sync')) {
            $this->load->view('permission/denied', array());
            return;
        }
        $list_kode_provinsi = $this->sekolah_model->get_list_kode_provinsi();
        $this->sekolah_model->delete_data_tabel();
        foreach ($list_kode_provinsi as $row => $data) {
            // var_dump($data['kode_wilayah']);
            // crawl data api untuk data sekolah
            // $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/Csekolah/detailSekolahGET?mst_kode_wilayah=020000';
            $url = 'http://jendela.data.kemdikbud.go.id/api/index.php/Csekolah/detailSekolahGET?mst_kode_wilayah=' . $data['kode_wilayah'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($curl);
            curl_close($curl);
            $result = $this->clearJSON($result);
            $array_sekolah = json_decode($result, true);

            // cek ada datanya tidak kalo ada di sinkronkan
            if (count($array_sekolah['data']) > 0) {
                $this->sekolah_model->sinkron_data($array_sekolah['data']);
            }
        }
        echo $result;
    }

    public function clearJSON($json_data)
    {
        if (0 === strpos(bin2hex($json_data), 'efbbbf')) {
            return substr($json_data, 3);
        } else {
            return $json_data;
        }
    }
}
