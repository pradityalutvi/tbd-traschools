<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permission extends App_Controller
{

    public function index()
    {
        if (!$this->ion_auth_acl->has_permission('hak_akses>read')) {
            $this->load->view('permission/denied', array());
            return;
        }

        $this->data['permissions'] = $this->ion_auth_acl->permissions('full');
        $this->data['title']       = 'Hak Akses';
        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Hak Akses', 'Hak Akses');
        $this->data['message_success'] = $this->session->flashdata('message_success');
        $this->layout->view('permission/permissions', $this->data);
    }

    public function add_permission()
    {
        // if (!$this->ion_auth_acl->has_permission('dashboard>pengaturan>hak_akses>create')) {
        //     $this->load->view('permission/denied', array());
        //     return;
        // }
        $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
        $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

        $this->form_validation->set_message('required', 'Please enter a %s');

        if ($this->form_validation->run() === FALSE) {
            $this->data['perm_key'] = array(
                'name' => 'perm_key',
                'id' => 'perm_key',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_key'),
                'class' => 'form-control',
                'autofocus' => 'autofocus',
            );

            $this->data['perm_name'] = array(
                'name' => 'perm_name',
                'id' => 'perm_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_name'),
                'class' => 'form-control',
                'autofocus' => 'autofocus',
            );

            $this->data['title']   = 'Tambah Hak Akses';
            $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Tambah Hak Akses', 'Hak Akses');
            $this->data['message'] = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));

            $this->layout->view('permission/add_permission', $this->data);
        } else {
            $new_permission_id = $this->ion_auth_acl->create_permission($this->input->post('perm_key'), $this->input->post('perm_name'));
            if ($new_permission_id) {
                // check to see if we are creating the permission
                // redirect them back to the admin page
                $this->session->set_flashdata('message_success', 'Hak akses telah berhasil dibuat!');
                redirect("permission", 'refresh');
            }
        }
    }

    public function update_permission()
    {
        // if (!$this->ion_auth_acl->has_permission('dashboard>pengaturan>hak_akses>update')) {
        //     $this->load->view('permission/denied', array());
        //     return;
        // }
        if ($this->input->post() && $this->input->post('cancel'))
            redirect("permission", 'refresh');

        $permission_id  =   $this->uri->segment(3);

        if (!$permission_id) {
            $this->session->set_flashdata('message_success', "No permission ID passed");
            redirect("permission", 'refresh');
        }

        $permission =   $this->ion_auth_acl->permission($permission_id);

        $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
        $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

        $this->form_validation->set_message('required', 'Please enter a %s');

        if ($this->form_validation->run() === FALSE) {

            $this->data['perm_key'] = array(
                'name' => 'perm_key',
                'id' => 'perm_key',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_key'),
                'class' => 'form-control',
                'value' => $permission->perm_key,
                'autofocus' => 'autofocus',
            );

            $this->data['perm_name'] = array(
                'name' => 'perm_name',
                'id' => 'perm_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_name'),
                'class' => 'form-control',
                'value' => $permission->perm_name,
                'autofocus' => 'autofocus',
            );

            $this->data['title']   = 'Tambah Hak Akses';
            $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Update Hak Akses', 'Hak Akses');
            $this->data['message']    = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));
            $this->data['permission'] = $permission;

            $this->layout->view('permission/edit_permission', $this->data);
        } else {
            $additional_data    =   array(
                'perm_name' =>  $this->input->post('perm_name')
            );

            $update_permission = $this->ion_auth_acl->update_permission($permission_id, $this->input->post('perm_key'), $additional_data);
            if ($update_permission) {
                // check to see if we are creating the permission
                // redirect them back to the admin page
                $this->session->set_flashdata('message_success', 'Hak akses telah berhasil diubah!');
                redirect(base_url("permission"), 'refresh');
            }
        }
    }

    public function delete_permission()
    {
        // if (!$this->ion_auth_acl->has_permission('dashboard>pengaturan>hak_akses>delete')) {
        //     $this->load->view('permission/denied', array());
        //     return;
        // }
        $permission_id  =   $this->uri->segment(3);

        if (!$permission_id) {
            $this->session->set_flashdata('message_success', "No permission ID passed");
            redirect("permission", 'refresh');
        }

        if ($this->ion_auth_acl->remove_permission($permission_id)) {
            $this->session->set_flashdata('message_success', 'Hak akses telah berhasil dihapus!');
            redirect("permission", 'refresh');
        } else {
            echo $this->ion_auth_acl->messages();
        }
    }
}
