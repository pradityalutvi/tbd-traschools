<?php
// header("Access-Control-Allow-Origin: *");
// header('Access-Control-Allow-Methods: GET, POST');
defined('BASEPATH') or exit('No direct script access allowed');

class Map_sekolah extends App_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tracking/map_sekolah_model');
    }

    public function index()
    {
        // header("Access-Control-Allow-Origin: *");
        if (!$this->ion_auth_acl->has_permission('track>school>view')) {
            $this->load->view('permission/denied', array());
            return;
        }
        $this->data = array('title' => 'Persebaran Sekolah');

        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Peta Sebaran Sekolah', 'Tracking');
        $this->data['form']        = $this->_populate_form();

        $this->layout->add_js(base_url('assets/js/tracking/map_sebaran_sekolah.js'));

        $this->layout->view('tracking/peta/sebaran_sekolah', $this->data);
    }

    public function _populate_form($data = array())
    {
        $req_sign = '<span class="text-danger"> * </span>';
        $list_provinsi = $this->map_sekolah_model->get_list_provinsi();
        $option_prov = array('' => 'Pilih Provinsi');
        foreach ($list_provinsi as $key => $prov) {
            $option_prov[$prov->kode_wilayah] = $prov->nama;
        }
        $form = array(
            'provinsi' => array(
                'label'  => 'Provinsi',
                'option' => $option_prov,
                'selected' => '',
                'extra' => array(
                    'id' => 'provinsi',
                    'name'   => 'provinsi',
                    'class' => 'dropdown-wilayah form-control app-select2'
                )
            ),
            'kabupaten' => array(
                'label'  => 'Kabupaten',
                'option' => array('' => 'Pilih Kabupaten'),
                'selected' => '',
                'extra' => array(
                    'id' => 'kabupaten',
                    'name'   => 'kabupaten',
                    'class' => 'dropdown-wilayah form-control app-select2'
                )
            ),
            'kecamatan' => array(
                'label'  => 'Kecamatan',
                'option' => array('' => 'Pilih Kecamatan'),
                'selected' => '',
                'extra' => array(
                    'id' => 'kecamatan',
                    'name'   => 'kecamatan',
                    'class' => 'form-control app-select2'
                )
            ),
            'jenjang' => array(
                'label'  => 'Jenjang',
                'option' => array(
                    '' => 'Pilih Jenjang',
                    'SD' => 'Sekolah Dasar',
                    'SMP' => 'Sekolah Menengah Pertama',
                    'SMA' => 'Sekolah Menengah Atas',
                    'SMK' => 'Sekolah Menengah Kejuruan'
                ),
                'selected' => '',
                'extra' => array(
                    'id' => 'jenjang',
                    'name'   => 'jenjang',
                    'class' => 'form-control app-select2'
                )
            ),
            'status' => array(
                'label'  => 'Status Negeri',
                'option' => array(
                    '' => 'Pilih Status',
                    'N' => 'Negeri',
                    'S' => 'Swasta'
                ),
                'selected' => '',
                'extra' => array(
                    'id' => 'status',
                    'name'   => 'status',
                    'class' => 'form-control app-select2'
                )
            ),

            'myLongitude' => array(
                'label'  => 'Longitude' . $req_sign,
                'input' => array(
                    'name'          => 'myLongitude',
                    'type'          => 'number',
                    'id'            => 'myLongitude',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Longitude Anda'
                )
            ),
            'myLatitude' => array(
                'label'  => 'Latitude' . $req_sign,
                'input' => array(
                    'name'          => 'myLatitude',
                    'type'          => 'number',
                    'id'            => 'myLatitude',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Latitude Anda'
                )
            )
        );

        //jika form gagal disimpan, set nilai form dari data post
        // foreach ($data as $key => $value) {
        //     $form[$key]['selected'] = $value;
        // }
        return $form;
    }

    public function get_list_kabupaten($kode_prov = null)
    {
        if (empty($kode_prov)) {
            echo json_encode(array());
            return;
        }
        $data_kab = $this->map_sekolah_model->get_list_kabupaten($kode_prov);
        echo json_encode($data_kab);
    }

    public function get_list_kecamatan($kode_kab = null)
    {
        if (empty($kode_kab)) {
            echo json_encode(array());
            return;
        }
        $data_kec = $this->map_sekolah_model->get_list_kecamatan($kode_kab);
        echo json_encode($data_kec);
    }

    public function get_marker_peta()
    {
        $search_by = $this->input->post();
        if (empty($search_by)) {
            echo json_encode(array());
            return;
        }
        $data['sekolah'] = $this->map_sekolah_model->get_data_peta($search_by);
        show(json_encode($data));
    }

    public function get_detail_sekolah($id_sekolah = null)
    {
        if (empty($id_sekolah)) {
            echo json_encode(array());
            return;
        }
        $detail_sekolah = $this->map_sekolah_model->get_detail_sekolah($id_sekolah);
        echo json_encode($detail_sekolah);
    }
}
