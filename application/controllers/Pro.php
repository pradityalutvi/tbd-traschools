<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pro extends App_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // header("Access-Control-Allow-Origin: *");
        $this->data = array('title' => 'Join Pro Member');

        $this->data['breadcrumbs'] = $this->layout->get_breadcrumbs('Daftar Pro Member', 'Pro Member');
        $this->layout->view('pro/index', $this->data);
    }
}
