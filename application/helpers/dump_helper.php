<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dump'))
{
    /**
     * menghasilkan output var_dump yang lebih mudah dibaca
     * @param  mixed   $data  data yang akan di dump
     * @param  boolean $exit  true jika exit setelah dump, false jika sebaliknya
     * @return null           tidak ada return value
     */
    function dump($data, $exit = true)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";

        if ($exit) {
            exit();
        }
    }
}