<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Map_sekolah_model extends CI_Model
{
    private $table = 'sekolah';
    private $table1 = 'provinsi';
    private $table2 = 'kabupaten';
    private $table3 = 'kecamatan';

    public function get_data_peta($search_by)
    {
        $query = $this->db->select('id, nama, jenjang, latitude, longitude')->from($this->table);
        $query->where("latitude is not null");
        $query->where("longitude is not null");

        if (!empty($search_by['kode_kecamatan'])) {
            $query->where('kode_kecamatan', $search_by['kode_kecamatan']);
        } else if (!empty($search_by['kode_kabupaten'])) {
            $query->where('kode_kabupaten', $search_by['kode_kabupaten']);
        } else if (!empty($search_by['kode_provinsi'])) {
            $query->where('kode_provinsi', $search_by['kode_provinsi']);
        }

        if ($search_by['kode_jenjang']) {
            $query->where('jenjang', $search_by['kode_jenjang']);
        }
        if ($search_by['kode_status']) {
            $query->where('status', $search_by['kode_status']);
        }
        return $query->get()->result();
    }

    public function get_list_provinsi()
    {
        $this->db->order_by('nama');
        return $this->db->select('kode_wilayah, nama')->get($this->table1)->result();
    }

    public function get_list_kabupaten($kode_prov = null)
    {
        $this->db->select('kode_wilayah, nama');
        $this->db->where('kode_provinsi', $kode_prov);
        return $this->db->get($this->table2)->result();
    }

    public function get_list_kecamatan($kode_kab = null)
    {
        $this->db->select('kode_wilayah, nama');
        $this->db->where('kode_kabupaten', $kode_kab);
        return $this->db->get($this->table3)->result();
    }

    public function get_detail_sekolah($id_sekolah = null)
    {
        $this->db->select('sekolah.nama, sekolah.alamat, sekolah.latitude, sekolah.longitude, sekolah.jenjang, sekolah.status, kecamatan.nama as nama_kecamatan, kabupaten.nama as nama_kabupaten, provinsi.nama as nama_provinsi');
        $this->db->join('provinsi', 'sekolah.kode_provinsi = provinsi.kode_wilayah', 'left');
        $this->db->join('kabupaten', 'sekolah.kode_kabupaten = kabupaten.kode_wilayah', 'left');
        $this->db->join('kecamatan', 'sekolah.kode_kecamatan = kecamatan.kode_wilayah', 'left');
        return $this->db->where('sekolah.id', $id_sekolah)->get($this->table)->result();
    }
}
