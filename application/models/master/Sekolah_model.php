<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sekolah_model extends CI_Model
{
    private $table = 'sekolah';
    private $table2 = 'provinsi';

    public function get_data_sekolah()
    {
        $this->load->library('datatables');

        $this->datatables->add_column('nomor', '');
        $this->db->order_by('id');
        $this->datatables->select('sekolah.nama, sekolah.alamat, sekolah.status, kabupaten.nama as nama_kabupaten,provinsi.nama as nama_provinsi');
        $this->datatables->from($this->table);
        $this->datatables->join('kabupaten', 'sekolah.kode_kabupaten = kabupaten.kode_wilayah', 'left');
        $this->datatables->join('provinsi', 'sekolah.kode_provinsi = provinsi.kode_wilayah', 'left');
        return $this->datatables->generate();
    }

    public function delete_data_tabel()
    {
        return $this->db->empty_table($this->table);
    }

    public function get_list_kode_provinsi()
    {
        return $this->db->select('kode_wilayah')->get($this->table2)->result_array();
    }
    public function sinkron_data($array_sekolah)
    {
        $id = 1;
        foreach ($array_sekolah as $row) {
            $this->db->set('id', $id);
            $this->db->set('nama', $row['sekolah']);
            $this->db->set('alamat', $row['alamat_jalan']);
            $this->db->set('latitude', $row['lintang']);
            $this->db->set('longitude', $row['bujur']);
            $this->db->set('jenjang', $row['bentuk']);
            $this->db->set('status', $row['status']);
            $this->db->set('kode_kecamatan', trim($row['kode_kec']));
            $this->db->set('kode_kabupaten', trim($row['kode_kab_kota']));
            $this->db->set('kode_provinsi', trim($row['kode_prop']));
            $this->db->insert($this->table);
            $id++;
        }
    }
}
