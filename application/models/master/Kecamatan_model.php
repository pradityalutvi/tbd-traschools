<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan_model extends CI_Model
{
    private $table = 'kecamatan';
    private $table2 = 'kabupaten';

    public function get_data_kecamatan()
    {
        $this->load->library('datatables');

        $this->datatables->add_column('nomor', '');
        $this->db->order_by('nama_kabupaten');
        $this->datatables->select('kecamatan.kode_wilayah, kecamatan.nama, kabupaten.nama as nama_kabupaten');
        $this->datatables->join('kabupaten', 'kecamatan.kode_kabupaten = kabupaten.kode_wilayah', 'left');
        $this->datatables->from($this->table);
        return $this->datatables->generate();
    }

    public function delete_data_tabel()
    {
        return $this->db->empty_table($this->table);
    }

    public function get_list_kode_kabupaten()
    {
        return $this->db->select('kode_wilayah')->get($this->table2)->result_array();
    }
    public function sinkron_data($array_wilayah)
    {
        foreach ($array_wilayah as $row) {
            // $input = array(
            //      'kode_wilayah' => $row['kode_wilayah'],
            //      'nama' => $row['nama'],
            //      'kode_provinsi' => $row['mst_kode_wilayah']
            // );
            // $this->db->insert($this->table, $input);
            //gini aja juga bisa jd gaperlu nyimpen array input
            // set(nama kolom tabel db, valuenya);
            $this->db->set('kode_wilayah', trim($row['kode_wilayah']));
            $this->db->set('nama', $row['nama']);
            $this->db->set('kode_kabupaten', trim($row['mst_kode_wilayah']));
            $this->db->insert($this->table);
        }
    }
}
