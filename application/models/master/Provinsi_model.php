<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi_model extends CI_Model
{
    private $table = 'provinsi';

    public function get_data_provinsi()
    {
        $this->db->order_by('kode_wilayah', 'ASC');
        return $this->db->get($this->table)->result();
    }

    public function delete_data_tabel()
    {
        return $this->db->empty_table($this->table);
    }

    public function sinkron_data($array_wilayah)
    {
        foreach ($array_wilayah as $row) {
            // $input = array(
            //     'kode_wilayah' => $row['kode_wilayah'],
            //     'nama' => $row['nama']
            // );
            // $this->db->insert($this->table, $input);
            //gini aja juga bisa jd gaperlu nyimpen array input
            // set(nama kolom tabel db, valuenya);
            // saya substr karena karakteristikanya "xxxxxx  " ada spasinya
            $this->db->set('kode_wilayah', trim($row['kode_wilayah']));
            $this->db->set('nama', $row['nama']);
            $this->db->insert($this->table);
        }
    }
}
