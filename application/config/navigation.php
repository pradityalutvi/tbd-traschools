<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Dashboard
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu (https://coreui.io/demo/#icons/simple-line-icons.html)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
| - permission : nama permission untuk hak akses menu
*/
$config['dashboard'] = array(
    'Beranda' => array(
        'icon' => 'nav-icon icon-home',
        'url' => base_url('dashboard'),
        'permission' => array(
            'beranda>read',
        ),
    ),
    'Data' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu' => array(
            'Provinsi'  => array(
                'url' => base_url('master/provinsi'),
                'icon' => 'nav-icon icon-docs',
                'permission' => 'master>provinsi>read'
            ),
            'Kabupaten'  => array(
                'url' => base_url('master/kabupaten'),
                'icon' => 'nav-icon icon-docs',
                'permission' => 'master>kabupaten>read'
            ),
            'Kecamatan'  => array(
                'url' => base_url('master/kecamatan'),
                'icon' => 'nav-icon icon-docs',
                'permission' => 'master>kecamatan>read'
            ),
            'Sekolah' => array(
                'url' => base_url('master/sekolah'),
                'icon' => 'nav-icon icon-docs',
                'permission' => 'master>sekolah>read'
            ),
        ),
        'permission' => array(
            'master>provinsi>read',
            'master>kabupaten>read',
            'master>kecamatan>read',
            'master>sekolah>read',
        ),
    ),
    'Tracking' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu' => array(
            'Persebaran Sekolah'  => array(
                'url' => base_url('tracking/map-sekolah'),
                'icon' => 'nav-icon icon-map',
                'permission' => 'track>school>view'
            ),
        ),
        'permission' => array(
            'track>school>view',
        ),
    ),

    'Others' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu' => array(
            'News'  => array(
                'url' => base_url('others/news'),
                'icon' => 'nav-icon icon-book-open',
                'permission' => 'others>news>read'
            ),
        ),
        'permission' => array(
            'others>news>read',
        ),
    ),

    'Daftar PRO' => array(
        'url' => base_url('pro'),
        'icon' => 'nav-icon icon-wallet',
        'permission' => array(
            'pro>create'
        ),
    ),

    'Pengaturan' => array(
        'icon' => 'nav-icon icon-settings',
        'submenu' => array(
            'Pengguna'  => array(
                'url' => base_url('auth'),
                'icon' => 'nav-icon icon-user',
                'permission' => 'user>read'
            ),
            'Hak Akses' => array(
                'url' => base_url('permission'),
                'icon' => 'nav-icon icon-lock',
                'permission' => 'hak_akses>read'
            ),
        ),
        'permission' => array(
            'user>read',
            'hak_akses>read',
        ),
    ),
);


/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Frontend
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu menggunakan font awesome (https://fontawesome.com/v4.7.0/icons)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
*/
$config['frontend'] = array();
