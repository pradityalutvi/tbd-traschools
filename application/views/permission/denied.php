<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Akses Ditolak!</title>
    <link rel="icon" href="<?php echo base_url('assets/img/brand/prd_icon.png') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
    <style type="text/css">
        #main {
            height: 100vh;
        }
    </style>
</head>

<body>
    <!------ Include the above in your HEAD tag ---------->

    <div class="d-flex flex-column justify-content-center align-items-center" id="main">
        <div class="d-flex flex-row justify-content-center align-items-center">
            <div>
                <img src="<?php echo base_url('assets/img/access_denied_2.png') ?>" width="250vh" class="p-2">
            </div>
            <div>
                <div class="inline-block align-middle p-2">
                    <h1 class="font-weight-normal lead" id="desc"><strong>Maaf</strong>, Anda dibatasi mengakses halaman ini! <br> Silahkan kembali ke laman sebelumnya.</h1>
                </div>
                <div class="p-2">
                    <a href="javascript:history.back()" class="btn btn-danger"><strong>Kembali</strong></a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>