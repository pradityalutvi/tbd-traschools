      <div class="row justify-content-center mt-3 mb-3 px-2">
        <div class="col-md-12 text-center">
          <img src="<?php echo base_url('assets/img/brand/logo.png') ?>" style="width: 65%;" />
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-md-10">


          <?php if (!empty($message_error)) : ?>
            <div class="alert alert-danger">
              <?php echo $message_error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php endif; ?>

          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <?php echo form_open('auth/login', 'class="needs-validation"'); ?>
                <h1>Login</h1>
                <p class="text-muted">Masukkan username dan password Anda.</p>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <?php echo form_input($identity); ?>
                  <?php echo form_error('identity', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <?php echo form_input($password); ?>
                  <?php echo form_error('password', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
                    <?php echo lang('login_remember_label', 'remember'); ?>
                  </div>
                  <div class="col-md-6 text-right">
                    <a class="inline-block" href="forgot_password">Lupa Password Anda?</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary btn-block px-4"'); ?>
                  </div>
                </div>
                <div class="row text-center">
                  <div class="col">
                    <hr>
                    <button class="btn btn-primary btn-block">Login dengan Facebook</button>
                  </div>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>
            <!-- <div class="flex-row align-items-center bg-primary text-white card py-5 d-md-down-none">
              <div class="card-body text-center">
                <div>
                  <h2>Lupa Password?</h2>
                  <p class="text-muted">Klik tombol berikut untuk me-reset password Anda, sistem kami akan mengirimkan tautan ke email Anda untuk me-reset password.</p>
                  <a class="btn btn-primary active mt-3 btn-block" href="forgot_password">
                    Reset Password
                  </a>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <div class="row" style="margin-top: 25px;">
        <div class="col-md-12 text-center m-1">
          <h5 class="d-md-down-none">Tugas Besar Matakuliah Teknologi Basis Data</h5>
          <h5><a class="shadow-lg" target="_blank" href="https://www.its.ac.id/matematika">Departemen Matematika - Institut Teknologi Sepuluh Nopember</a></h5>
        </div>
      </div>