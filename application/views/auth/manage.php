<h1>Manage</h1>

<?php echo anchor('/dashboard', "Dashboard"); ?>

<ul>
    <li><?php echo anchor('/auth/permissions', "Manage Permission's"); ?></li>
    <li><?php echo anchor('/auth/groups', "Manage Group's"); ?></li>
    <li><?php echo anchor('/auth/users', "Manage User's"); ?></li>
</ul>
