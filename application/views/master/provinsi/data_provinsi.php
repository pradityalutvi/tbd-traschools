<div class="row">
    <div class="col-md-12">

        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-docs"></i> <?php echo show($title); ?>
                <div class="pull-right">
                    <?php if ($this->ion_auth_acl->has_permission('master>provinsi>sync')) : ?>
                        <button id="sync" class="btn btn-primary btn-sm"><i class="nav-icon icon-plus"></i> Sinkron Data Kemendikbud</button>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed prov-datatable table-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Kode Wilayah</th>
                                        <th>Nama Provinsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($list_data as $key => $data) :
                                    ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no++; ?></td>
                                            <td><?php show($data->kode_wilayah); ?></td>
                                            <td><?php show($data->nama); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Konfirmasi modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-warning">WARNING</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Sistem akan melakukan sinkronisasi data dengan pihak kemendikbud, sehingga data lama anda ada kemungkinan akan hilang atau digantikan.</h6>
                <p class="text-danger"><small>NB : <span class="font-weight-bold">Proses sinkronisasi mungkin membutuhkan waktu yang cukup lama.</span> Pastikan koneksi internet stabil.</small></p>
                <div class="row justify-content-center">
                    <div class="col text-center">
                        <button type="button" data-dismiss="modal" class="btn btn-primary" id="yesConfirm">Sinkronkan</button>
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- konfirmasi -->

<!-- Modal Info Sukses-->
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sinkronisasi Selesai</h5>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col text-center">
                        <span style="text-decoration: blink;" id="pesan-sync"></span>
                        <!-- <p>Sinkronisasi telah selesai. Silahkan reload laman untuk melanjutkan.</p> -->
                        <a href="<?php echo base_url('master/provinsi/'); ?>" class="btn btn-primary btn-sm">Reload</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->