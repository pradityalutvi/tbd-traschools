<?php
$form_label = array('class' => 'control-label');
?>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-map"></i> <?php echo show($title); ?>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_open('', array()); ?>
                                <div class="row">

                                    <div class="form-group col-md-4">
                                        <?php echo form_label($form['provinsi']['label'], 'provinsi', $form_label) ?>
                                        <?php echo form_dropdown($form['provinsi']['extra'], $form['provinsi']['option'], $form['provinsi']['selected']); ?>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <?php echo form_label($form['kabupaten']['label'], 'kabupaten', $form_label) ?>
                                        <?php echo form_dropdown($form['kabupaten']['extra'], $form['kabupaten']['option'], $form['kabupaten']['selected']); ?>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <?php echo form_label($form['kecamatan']['label'], 'kecamatan', $form_label) ?>
                                        <?php echo form_dropdown($form['kecamatan']['extra'], $form['kecamatan']['option'], $form['kecamatan']['selected']); ?>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="form-group col-md-4">
                                        <?php echo form_label($form['jenjang']['label'], 'jenjang', $form_label) ?>
                                        <?php echo form_dropdown($form['jenjang']['extra'], $form['jenjang']['option'], $form['jenjang']['selected']); ?>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <?php echo form_label($form['status']['label'], 'status', $form_label) ?>
                                        <?php echo form_dropdown($form['status']['extra'], $form['status']['option'], $form['status']['selected']); ?>
                                    </div>

                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="mapid" style="height: 400px;"></div>
                        <?php if ($this->ion_auth_acl->has_permission('track>school>near_me')) : ?>
                            <div class="row mt-2">
                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?php echo form_label($form['myLatitude']['label'], 'myLatitude', $form_label) ?> <br>
                                            <small class="text-info"><span class="font-weight-bold">Contoh</span> : -6.7142787</small>
                                        </div>
                                        <div class="col-md-8"><?php echo form_input($form['myLatitude']['input']); ?></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?php echo form_label($form['myLongitude']['label'], 'myLongitude', $form_label) ?> <br>
                                            <small class="text-info"><span class="font-weight-bold">Contoh</span> : 111.4542608</small>
                                        </div>
                                        <div class="col-md-8"><?php echo form_input($form['myLongitude']['input']); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <small class="text-danger"><span class="font-weight-bold">NB : *)</span> harus diisi untuk menggunakan fiture locate me.</small>
                                <div class="pull-right">
                                    <button id="locateMe" class="btn btn-sm btn-primary"><i class="fa fa-map-pin"></i> Locate Me</button>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal Details-->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Sekolah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="detail_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />