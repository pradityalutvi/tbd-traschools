<!DOCTYPE html>
<html lang="id">

<head>
  <link rel="icon" href="<?= base_url('assets/img/brand/prd_icon.png'); ?>">
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title><?php echo show($title) . ' | ' . $this->config->item('app_title'); ?></title>
  <style type="text/css">
    .loader {
      display: none;
      position: fixed;
      width: 100%;
      height: 100%;
      z-index: 999;
      /* background-color: rgba(100, 100, 255, 0.7); */
    }

    .loader div {
      text-align: center;
      display: table-cell;
      vertical-align: middle;
    }

    @media (max-width: 991.98px) {
      .view-full {
        display: none !important;
      }

      .view-small {
        display: block !important;
      }
    }
  </style>
  <?php foreach ($list_css as $key_css => $url_css) : ?>
    <link href="<?php echo $url_css; ?>" rel="stylesheet">
  <?php endforeach; ?>
</head>

<!-- modif dikit -->
<div id="loader" class="loader">
  <div>
    <img width="150px" src="<?php echo base_url('assets/img/loader2.gif'); ?>">
    <h4 class="text-dark font-weight-bold">Mohon sabar menunggu...!</h4>
  </div>
</div>

<body class="app header-fixed sidebar-fixed aside-menu-fixed <?php echo (current_url() != base_url()) ? 'sidebar-lg-show' : '' ?>">

  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= base_url(); ?>">
      <img class="navbar-brand-full" src="<?php echo base_url('assets/img/brand/logo.png'); ?>" width="150" alt="CoreUI Logo">
      <img class="navbar-brand-minimized" src="<?php echo base_url('assets/img/brand/sygnet.png'); ?>" height="40" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!--  <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Dashboard</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Settings</a>
        </li>
      </ul> -->
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <strong class="view-full" style="display: inline-block;"><?php show($this->ion_auth->user()->row()->first_name . '  (' . $this->ion_auth->get_users_groups()->row()->description . ')'); ?></strong>
          <div class="img-avatar" style="display: inline-block; margin-bottom: -12px; width: 36px; height: 36px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo base_url('assets/img/avatars/user.png'); ?>);"></div>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <div class="dropdown-header text-center">
            <strong>User Login</strong>
          </div>
          <a class="dropdown-item" href="<?php echo base_url('auth/change-password'); ?>">
            <i class="fa fa-lock"></i> Ganti Password
          </a>
          <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>">
            <i class="fa fa-sign-out"></i> Logout
          </a>
        </div>
      </li>
    </ul>
  </header>
  <div class="app-body">
    <div class="sidebar">
      <nav class="sidebar-nav">
        <ul class="nav">

          <li class="nav-title">Menu Apps</li>

          <?php foreach ($dashboard_menu as $title => $menu) : ?>

            <?php
            $cek_permission = FALSE;
            foreach ($menu['permission'] as $key) {
              $cek_permission = $cek_permission || $this->ion_auth_acl->has_permission($key);
            }
            if ($cek_permission) :
            ?>

              <li class="nav-item nav-dropdown <?php echo !empty($menu['submenu']) ? expand_menu($menu['submenu']) : ''; ?>">

                <!-- nama menu -->
                <a class="nav-link <?php echo !empty($menu['submenu']) ? 'nav-dropdown-toggle' : ''; ?>" href="<?php echo !empty($menu['url']) ? $menu['url'] : '#'; ?>">
                  <i class="<?php echo $menu['icon']; ?>"></i> <?php echo $title; ?>
                </a>

                <!-- sub-menu -->
                <?php if (!empty($menu['submenu'])) : ?>

                  <ul class="nav-dropdown-items">

                    <?php foreach ($menu['submenu'] as $submenu_title => $submenu) : ?>
                      <?php if ($this->ion_auth_acl->has_permission($submenu['permission'])) : ?>
                        <li class="nav-item">
                          <a class="nav-link <?php echo !empty($submenu['url']) ? active_menu($submenu['url']) : ''; ?>" href="<?php echo $submenu['url']; ?>">
                            <i class="<?php echo $submenu['icon']; ?>"></i> <?php echo $submenu_title; ?>
                          </a>
                        </li>
                      <?php endif ?>
                    <?php endforeach; ?>

                  </ul>

                <?php endif; ?>

              </li>

            <?php endif ?>
          <?php endforeach; ?>

        </ul>
      </nav>
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
      <!-- Breadcrumb-->
      <ol class="breadcrumb">
        <?php
        if (isset($breadcrumbs)) :
          foreach ($breadcrumbs as $key_bread => $breadcrumb) :
        ?>

            <?php
            end($breadcrumbs);
            if ($key_bread == key($breadcrumbs)) :
            ?>
              <li class="breadcrumb-item">
                <?php echo $breadcrumb['title'] ?>
              </li>
            <?php else : ?>
              <li class="breadcrumb-item">
                <a href="<?php echo $breadcrumb['url']; ?>"><?php echo $breadcrumb['title'] ?></a>
              </li>
        <?php
            endif;
          endforeach;
        endif;
        ?>
        &nbsp;
        <!-- Breadcrumb Menu-->
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <?php echo $content; ?>
        </div>
      </div>
    </main>
    <aside class="aside-menu">
      <!-- Tab panes-->
    </aside>
  </div>
  <footer class="app-footer">
    <div>
      <span>&copy;Built by : <a href="https://www.linkedin.com/in/pradityalutvi/" target="_blank">Praditya L.</a>, <a href="https://www.linkedin.com/in/pradityalutvi/" target="_blank">Fachri F.</a>, <a href="https://www.linkedin.com/in/pradityalutvi/" target="_blank">Aguns.</a></span>
    </div>
    <div class="ml-auto">
      <span>Templated by</span>
      <a href="https://coreui.io" target="_blank">CoreUI</a>
    </div>
  </footer>

  <script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
  </script>

  <!-- generate js yang akan di load di halaman backend -->
  <?php foreach ($list_js as $key_js => $url_js) : ?>
    <script src="<?php echo $url_js; ?>"></script>
  <?php endforeach; ?>

</body>

</html>