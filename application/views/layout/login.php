<!DOCTYPE html>
<html lang="id">

<head>
  <link rel="icon" href="<?= base_url('assets/img/brand/prd_icon.png'); ?>">
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title><?php echo $title . ' | ' . $this->config->item('app_title'); ?></title>
  <!-- Icons-->
  <link href="<?php echo base_url('assets/vendors/@coreui/icons/css/coreui-icons.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/vendors/flag-icon-css/css/flag-icon.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/vendors/simple-line-icons/css/simple-line-icons.css'); ?>" rel="stylesheet">
  <!-- Main styles for this application-->
  <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/vendors/pace-progress/css/pace.min.css'); ?>" rel="stylesheet">
</head>

<body class="app flex-row align-items-center justify-content-center" style="height:100vh;">
  <div class="container" style="display:block; position:absolute; margin: 0; padding: 0; width:60%;">
    <?php echo $content; ?>
  </div>
  <!-- particles.js container -->
  <div id="particles-js" style="width:100%; height:100vh; background-image: url(<?php echo base_url('assets/img/login-background.jpg'); ?>); background-size: cover; background-repeat: no-repeat;"></div>

  <!-- particles.js lib (JavaScript CodePen settings): https://github.com/VincentGarreau/particles.js -->
  <!-- CoreUI and necessary plugins-->
  <script src="<?php echo base_url('assets/vendors/jquery/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/popper.js/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/pace-progress/js/pace.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/@coreui/coreui/js/coreui.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendors/particles.js/particles.min.js'); ?>"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      particlesJS.load('particles-js', '<?php echo base_url('assets/vendors/particles.js/particlesjs-config.json'); ?>');
    });
  </script>
</body>

</html>