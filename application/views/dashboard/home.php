<div class="row">
	<div class="col-md-12">
		<div class="jumbotron bg-light">
			<div class="container">
				<h1 class="display-5 text-dark text-capitalize">Selamat Datang, <?php echo $this->ion_auth->user()->row()->first_name; ?> <?php echo $this->ion_auth->user()->row()->last_name; ?></h1>
				<p class="lead text-dark">Traschools - Geo Tracking Schools Information</p>
				<?php if ($this->ion_auth_acl->has_permission('track>school>view')) : ?>
					<a href="<?php echo base_url('tracking/map_sekolah/'); ?>" class="btn btn-primary btn-md"><i class="nav-icon icon-map"></i> View Schools Map</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>