$('body').on("click", "#sync", function (e) {
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    })
        .on('click', '#yesConfirm', function (e) {
            ajaxSyncKemendikbud();
        });
});


function ajaxSyncKemendikbud() {
    $.ajax({
        url: base_url + 'master/provinsi/sync_data_kemendikbud/000000',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            $('#sync').hide();
            $("#loader").css("display", "table");
            // $("#loading-text").html('<i class="nav-icon icon-clock"></i> <strong>Sinkronisasi Data..</strong>');
        },
        success: function (response) {
            var pesan = '<h6 class="text-success">Sinkronisasi telah selesai. Silahkan reload laman untuk melanjutkan.</h6>';
            $("#pesan-sync").html(pesan);
        },
        complete: function () {
            // $('#loading-text').html("");
            $("#loader").css("display", "none");
            $('#sync').show();
            //agar focus ke modal sebelum modal ditutup
            $('#modalInfo').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }).fail(function () {
        var pesan = '<h6 class="text-danger">Sinkronisasi belum sepenuhnya selesai. Pastikan jaringan internet stabil untuk mengulangi sinkronisasi.</h6>';
        $("#pesan-sync").html(pesan);
    });
}