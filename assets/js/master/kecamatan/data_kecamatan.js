// get data kecamatan from db lewat ajax
$('.kecamatan-datatable').DataTable({
    'responsive': true,
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": base_url + 'master/kecamatan/get_data_kecamatan',
        "type": "POST"
    },
    //Set column definition initialisation properties.
    "columns": [
        { "data": "nomor" },
        { "data": "kode_wilayah" },
        { "data": "nama" },
        { "data": "nama_kabupaten" },
    ],

    "fnCreatedRow": function (row, data, index) {
        var info = $(this).DataTable().page.info();
        var value = index + 1 + info.start;
        $('td', row).eq(0).html(value).attr('align', 'center');
    },

    "language": {
        "url": base_url + "assets/vendors/datatables/indonesia.json"
    },
});

//proses sinkronisasi
$('body').on("click", "#sync", function (e) {
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    })
        .on('click', '#yesConfirm', function (e) {
            ajaxSyncKemendikbud();
        });
});

function ajaxSyncKemendikbud() {
    $.ajax({
        url: base_url + 'master/kecamatan/sync_data_kemendikbud/',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            $('#sync').hide();
            $("#loader").css("display", "table");
            // $("#loading-text").html('<i class="nav-icon icon-clock"></i> <strong>Sinkronisasi Data..</strong>');
        },
        success: function (response) {
            // console.log(response);
            var pesan = '<h6 class="text-success">Sinkronisasi telah selesai. Silahkan reload laman untuk melanjutkan.</h6>';
            $("#pesan-sync").html(pesan);
        },
        complete: function () {
            // $("#loading-text").html("");
            $("#loader").css("display", "none");
            $('#sync').show();
            //agar focus ke modal sebelum modal ditutup

            $('#modalInfo').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }).fail(function () {
        var pesan = '<h6 class="text-danger">Sinkronisasi belum sepenuhnya selesai. Pastikan jaringan internet stabil untuk mengulangi sinkronisasi.</h6>';
        $("#pesan-sync").html(pesan);
    });
}