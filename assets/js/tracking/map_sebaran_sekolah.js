var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicHJhZGl0eWEiLCJhIjoiY2s5empibHo0MGVwZjNubzBrNjFpZW9qMyJ9.IhEFgSdb9k-FGjFPt8HZcg';

var street = L.tileLayer(mbUrl, { id: 'mapbox/outdoors-v11', attribution: mbAttr }),
    satellite = L.tileLayer(mbUrl, { id: 'mapbox/satellite-streets-v11', attribution: mbAttr });

var baseMaps = {
    "Street": street,
    "Satellite": satellite,
};

var mymap = L.map('mapid', {
    center: [-2.3256, 118.0053],
    zoom: 5,
    layers: [street],
    zoomControl: false,
    fullscreenControl: true,
});
L.control.zoom({ position: 'bottomright' }).addTo(mymap);
L.control.layers(baseMaps).addTo(mymap);
var layer_grup = L.layerGroup().addTo(mymap);
var myIconBlack = L.icon({ iconUrl: base_url + 'assets/img/gps/icon_black.png', iconSize: [30, 30] }); //untuk smk
var myIconBlue = L.icon({ iconUrl: base_url + 'assets/img/gps/icon_blue.png', iconSize: [30, 30] }); // untuk sma
var myIconGreen = L.icon({ iconUrl: base_url + 'assets/img/gps/icon_green.png', iconSize: [30, 30] }); // untuk sd
var myIconRed = L.icon({ iconUrl: base_url + 'assets/img/gps/icon_red.png', iconSize: [30, 30] }); // untuk smp

// load_marker();
$("body").on("change", "#provinsi", function (e) {
    var kode_wil = $(this).val();
    layer_grup.clearLayers();
    $.ajax({
        url: base_url + 'tracking/map_sekolah/get_list_kabupaten/' + kode_wil,
        type: 'post',
        dataType: 'json',
        beforeSend: function (e) {
            $("#kabupaten option").remove();
            $("#kabupaten").append('<option value="">Pilih Kabupaten</option>');

            $("#kecamatan option").remove();
            $("#kecamatan").append('<option value="">Pilih Kecamatan</option>');
        },
        success: function (response) {
            $.each(response, function (index, dataKabupaten) {
                $('#kabupaten').append('<option value="' + dataKabupaten.kode_wilayah + '">' + dataKabupaten.nama + '</option>');
            });
            load_marker();
        }
    });
});
$("body").on("change", "#kabupaten", function (e) {
    var kode_wil = $(this).val();

    layer_grup.clearLayers();
    $.ajax({
        url: base_url + 'tracking/map_sekolah/get_list_kecamatan/' + kode_wil,
        type: 'post',
        dataType: 'json',
        beforeSend: function (e) {
            $("#kecamatan option").remove();
            $("#kecamatan").append('<option value="">Pilih Kecamatan</option>');
        },
        success: function (response) {
            $.each(response, function (index, dataKec) {
                $("#kecamatan").append('<option value="' + dataKec.kode_wilayah + '">' + dataKec.nama + '</option>');
            });
            load_marker();
        }
    });
});

$("body").on("change", "#kecamatan", function () {
    layer_grup.clearLayers();
    load_marker();
});
$("body").on("change", "#jenjang", function () {
    layer_grup.clearLayers();
    load_marker();
});

$("body").on("change", "#status", function () {
    layer_grup.clearLayers();
    load_marker();
});

$("body").on("click", "#locateMe", function (e) {
    var longi = $("#myLongitude").val();
    var lati = $("#myLatitude").val();
    if (lati.length < 1 || longi.length < 1) {
        console.log("data kurang");
    } else {
        // console.log(lati + ", " + longi);
        var iconLocateMe = L.icon({ iconUrl: base_url + 'assets/img/gps/locate-me.png', iconSize: [35, 35] });
        markerMe = new L.marker([lati, longi], { icon: iconLocateMe }).addTo(layer_grup);
        circle = L.circle([lati, longi], 700, {
            color: 'blue',
            fillColor: 'blue',
            fillOpacity: 0.3
        }).addTo(layer_grup);
        mymap.flyTo([lati, longi], 15, { duration: 1.5,});
        // centerLeafletMapOnMarker(mymap, markerMe);
        // $("#myLongitude").val(null)
        // $("#myLatitude").val(null);
    }
});
// function centerLeafletMapOnMarker(map, marker) {
//     var latLngs = [marker.getLatLng()];
//     // var markerBounds = L.latLngBounds(latLngs);
//     map.flyTo(latLngs, 10);
// }

function load_marker() {
    var prov = $("#provinsi").val();
    var kab = $("#kabupaten").val();
    var kec = $("#kecamatan").val();
    var jenjang = $("#jenjang").val();
    var status = $("#status").val();

    $.ajax({
        url: base_url + 'tracking/map_sekolah/get_marker_peta/',
        type: 'post',
        dataType: 'json',
        data: { kode_provinsi: prov, kode_kabupaten: kab, kode_kecamatan: kec, kode_jenjang: jenjang, kode_status: status },
        beforeSend: function () {
            $("#loader").css("display", "table");
        },
        success: function (response) {
            var data_sekolah = response.sekolah;
            // console.log(data_sekolah);
            for (var i = 0; i < data_sekolah.length; i++) {
                if (data_sekolah[i].jenjang == "SD") {
                    marker = new L.marker([data_sekolah[i].latitude, data_sekolah[i].longitude], { icon: myIconGreen }).bindPopup(
                        `<a data-toggle="modal"  onclick="detailSekolah(` + data_sekolah[i].id + `)" class="" id="detail" href="#">` + data_sekolah[i].nama + `</a>`)
                        .openPopup().addTo(layer_grup);
                }
                else if (data_sekolah[i].jenjang == "SMP") {
                    marker = new L.marker([data_sekolah[i].latitude, data_sekolah[i].longitude], { icon: myIconRed }).bindPopup(
                        `<a data-toggle="modal"  onclick="detailSekolah(` + data_sekolah[i].id + `)" class="" id="detail" href="#">` + data_sekolah[i].nama + `</a>`)
                        .openPopup().addTo(layer_grup);
                }
                else if (data_sekolah[i].jenjang == "SMK") {
                    marker = new L.marker([data_sekolah[i].latitude, data_sekolah[i].longitude], { icon: myIconBlack }).bindPopup(
                        `<a data-toggle="modal"  onclick="detailSekolah(` + data_sekolah[i].id + `)" class="" id="detail" href="#">` + data_sekolah[i].nama + `</a>`)
                        .openPopup().addTo(layer_grup);
                }
                else {
                    marker = new L.marker([data_sekolah[i].latitude, data_sekolah[i].longitude], { icon: myIconBlue }).bindPopup(
                        `<a data-toggle="modal"  onclick="detailSekolah(` + data_sekolah[i].id + `)" class="" id="detail" href="#">` + data_sekolah[i].nama + `</a>`)
                        .openPopup().addTo(layer_grup);
                }
                // marker = new L.marker([data_sekolah[i].latitude, data_sekolah[i].longitude], { icon: myIconBlue }).bindPopup(coba).openPopup().addTo(layer_grup)
            }
        },
        complete: function () {
            $("#loader").css("display", "none");
        }
    });
}

function detailSekolah(idSekolah) {
    // console.log(idSekolah);
    $.ajax({
        url: base_url + 'tracking/map_sekolah/get_detail_sekolah/' + idSekolah,
        type: 'POST',
        dataType: 'json',
        success: function (data_sekolah) {
            // console.log(data_sekolah);
            if (data_sekolah[0].nama != null) {
                var content = `
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <img style="width:100vh;" src="`+ base_url + 'assets/img/foto-sekolah/dummy.png' + `" class="img-fluid">
                        </div>
                        <div class="col-md-8">
                            <ul class="list-group">
                                <li class="list-group-item"><h4>`+ data_sekolah[0].nama + `</h4></li>
                                <li class="list-group-item">Alamat : `+ data_sekolah[0].alamat + `</li>
                                <li class="list-group-item">Latitude : `+ data_sekolah[0].latitude + `</li>
                                <li class="list-group-item">Longitude : `+ data_sekolah[0].longitude + `</li>
                                <li class="list-group-item">Jenjang : `+ data_sekolah[0].jenjang + `</li>
                                <li class="list-group-item">Status Negeri : `+ data_sekolah[0].status + `</li>
                                <li class="list-group-item">Kecamatan : `+ data_sekolah[0].nama_kecamatan + `</li>
                                <li class="list-group-item">Kabupaten : `+ data_sekolah[0].nama_kabupaten + `</li>
                                <li class="list-group-item">Provinsi : `+ data_sekolah[0].nama_provinsi + `</li>
                            </ul>
                        </div>
                    </div>
                </div>
                `;
                $('.modal-body').html(content);
            }
        },
        complete: function () {
            $('#modalDetail').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
}